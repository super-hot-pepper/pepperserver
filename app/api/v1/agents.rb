module V1
  class Agents < Grape::API
    resources :agents do
      params do
        requires :agent_id, type: Integer
      end
      get '/:agent_id' do
        @agent = Agent.where(id: params[:agent_id]).first
        error!({message: "Agent Not Found"}, 404) unless @agent
        {
          agent_id: @agent.id,
          name: @agent.name,
          side: @agent.side.name,
          fource: @agent.fource,
          level: @agent.level,
          ex: @agent.ex
        }
      end

      params do
        requires :agent_id, type: Integer
        requires :pepper_id, type: Integer
      end
      get '/:agent_id/fight/:pepper_id' do
        @agent = Agent.where(id: params[:agent_id]).first
        @pepper = Pepper.where(id: params[:pepper_id]).first
      end
    end
  end
end
