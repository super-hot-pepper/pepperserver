module V1
  class Peppers < Grape::API
    resource :peppers do
      desc "all peppers"
      params do
        optional :loc, type: Hash do
          requires :lat, type: Float
          requires :long, type: Float
          optional :dist, type: Float
        end
      end
      get '/', jbuilder: 'peppers/index' do
        if params[:loc] then
          dist = 3
          dist = params[:loc][:dist] if params[:loc][:dist]
          @peppers = Pepper.includes(:pepper_side => :side).within_box(dist, params[:loc][:lat], params[:loc][:long])
        else
          @peppers = Pepper.includes(:pepper_side => :side).all
        end
      end

      params do
        requires :pepper_id, type: Integer, desc: "Pepper ID"
      end
      get '/:pepper_id/state' do
        @pepper = Pepper.where(id: params[:pepper_id]).first
        {
          pepper_id: @pepper.id,
          heart_points: @pepper.heart_points,
          side: @pepper.side.name,
          lat: @pepper.lat,
          long: @pepper.long,
          last_agent_id: @pepper.last_agent_id,
          last_fight_time: @pepper.last_fight_time
        }
      end

      desc "change heart points and states"
      params do
        requires :pepper_id, type: Integer, desc: 'Pepper ID'
        requires :agent_id, type: Integer, desc: 'Agent ID'
        requires :result, type: Integer, desc: 'Result of the game' # 0: agent lost, 1: agent won
        requires :score, type: Integer, desc: 'Score of the game'
      end
      put '/:pepper_id/state' do
        @pepper = Pepper.where(id: params[:pepper_id]).first
        @agent = Agent.where(id: params[:agent_id]).first
        @score = params[:score].to_i
        if !(@pepper && @agent) then
          error!({message: "Pepper or Agent Not Found"}, 404)
        end
        @result = params[:result].to_i
        if @result != 0 or @result != 1 then
          error!({message: "Invalid result"}, 500)
        end

        if @result == 0 then
          direction = (@agent.side.name == "light") ? 1 : -1
          ex_mag = Math.log10(10 + @pepper.heart_points.abs)
          if @pepper.side.name == "neutral" then
            @pepper.heart_points += @agent.force * direction
          elsif @pepper.side == @agent.side then
            @pepper.heart_points += @agent.force * direction
          else
            @pepper.heart_points -= @agent.force * direction
          end

          # update side
          if @pepper.heart_points < -10 then
            @pepper.pepper_side = PepperSide.find(2) # Dark side
          elsif @pepper.heart_points > 10 then
            @pepper.pepper_side = PepperSide.find(3) # Light side
          else
            @pepper.pepper_side = PepperSide.find(1)
          end
          @agent.force += @score * @agent.level / 100
          if @agent.force > @agent.level * 100 then
            @agent.force = @agent.level * 100
          end
          @agent.ex += 10 * ex_mag
          if @agent.ex > @agent.level * @agent.level * 30 then
            @agent.ex = 0
            @agent.level  += 1
          end
        else
          @agent.force = 0
        end
        @pepper.last_agent_id = @agent.id
        @pepper.last_fight_time = DateTime.now

        if @pepper.save && @agent.save
          {
            pepper_id: @pepper.id,
            heart_points: @pepper.heart_points,
            side: @pepper.side.name,
            lat: @pepper.lat,
            long: @pepper.long,
            last_agent_id: @pepper.last_agent_id,
            last_fight_time: @pepper.last_fight_time
          }
        else
          error!({message: "something went wrong"},500)
        end
      end
    end
  end
end
