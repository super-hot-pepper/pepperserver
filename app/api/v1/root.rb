module V1
  class Root < Grape::API
    version 'v1'
    mount V1::Peppers
    # mount V1::PepperSides
    # mount V1::Agents
    # mount V1::AgentSides
    # mount V1::Items
  end
end
