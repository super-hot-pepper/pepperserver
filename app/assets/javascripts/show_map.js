var center_lat = 35.620955;
var center_long = 139.748678;
var minLat = center_lat - 0.01;
var maxLat = center_lat + 0.01;
var midLat = (maxLat + minLat)/2;

var minLng = center_long - 0.01;
var maxLng = center_long + 0.01;
var midLng = (minLng + maxLng)/2;

var particleDuration = 120000;  // ms

var peppers = {};
var pepperArray = [];
var N = 100;

// var socket = io.connect('http://ec2-52-68-34-134.ap-northeast-1.compute.amazonaws.com:8080');

// socket.on('connect', function(msg) {
  // console.log('connected');
// });

// peppers field
// device: "android" or "iphone"
// sex: 0 or 1 or 2 (0 is male, 1 is female, 2 is unknown)
// longitude
// latitude
// user_uuid: string
// date: 2015-05-15-23:39:13

function pull() {
  $.get('/api/v1/peppers', function(data){
    console.log(data.peppers.length);
    pepperArray = data.peppers;
  });
}

function init() {
  var container = document.getElementById('map-div');
  pull();

  var map = new google.maps.Map(container, {
    mapTypeControl: false,
    styles: styles,
    center: new google.maps.LatLng(center_lat, center_long),
    zoom: 17,
    mapTypeId: google.maps.MapTypeId.ROADMAP,
    // panControl: false,
    // draggable: false,
    // disableDefaultUI: true,
    minZoom: 1,
    maxZoom: 17
  });

  var marker = new google.maps.Marker({
    position: new google.maps.LatLng(center_lat, center_long),
    map: map,
    title: 'Your current location'
  });

  // if you add renderertype:'Canvas' to the options for ThreejsLayer, you can force the usage of CanvasRenderer
  new ThreejsLayer({ map: map }, function(layer){

    if (layer.renderertype=='Canvas' || !Detector.webgl) {
      console.log("fuck"); // in this application don't give a shit to non WebGL supporting browsers
    }

    var geometry = new THREE.Geometry(),
    texture = new THREE.Texture(generateSprite()),
    material, particles;

    for(var i=0; i<N; i++){
      var vertex = new THREE.Vector3(-10, -10, 0);
      var color = new THREE.Color(255, 255, 255);
      geometry.vertices.push(vertex);
      geometry.colors.push(color);
    }

    geometry.verticesNeedUpdate = true;
    // geometry.elementsNeedUpdate = true;
    geometry.colorsNeedUpdate = true;
    // geometry.dynamic = true;
    texture.needsUpdate = true;

    material = new THREE.PointCloudMaterial({
      size: 70,
      map: texture,
      opacity: 1,
      blending: THREE.AdditiveBlending,
      depthTest: false,
      transparent: true,
      vertexColors: true
    });

    particles = new THREE.PointCloud( geometry, material );

    layer.add( particles );

    console.log(particles);

    // gui = new dat.GUI();

    pepperArray.forEach(function(datum){
      var pepper = peppers[datum.id];
      if(!pepper){
        // new person
        // console.log(datum.lat, datum.long);
        var location = new google.maps.LatLng(parseFloat(datum.lat), parseFloat(datum.long));
        var vertex = layer.fromLatLngToVertex(location);
        // vertex.user_uuid = datum.user_uuid;
        var color;
        var side = datum.side;
        if(side == "light"){
          color = new THREE.Color(0x0099FF);
        }else if(side == "dark"){
         color = new THREE.Color(0xFF0000);
        }else {
          color = new THREE.Color(0x666666);
        }
        for(var i=0; i<N; i++){
          var _vertex = geometry.vertices[i];
          if(!_vertex.pepper_id){
            // _vertex = vertex;
            _vertex.pepper_id = datum.id;
            _vertex.x = vertex.x;
            _vertex.y = vertex.y;
            // console.log("new person at the index of " + i);
            geometry.colors[i] = color;
            break;
          }
        }

        geometry.verticesNeedUpdate = true;
        geometry.colorsNeedUpdate = true;
      }
      peppers[datum.id] = {
        pepper_id: datum.id,
        // device: datum.device,
        side: datum.side,
        long: parseFloat(datum.long),
        lat: parseFloat(datum.lat)
      };
    });
    // socket.on("publish", function(data){
      // // console.log(geometry.vertices.length);
      // // console.log(data.length);
      // data.forEach(function(datum){
        // var person = peppers[datum.user_uuid];
        // if(!person){
          // // new person
          // var location = new google.maps.LatLng(parseFloat(datum.latitude), parseFloat(datum.longitude));
          // var vertex = layer.fromLatLngToVertex(location);
          // // vertex.user_uuid = datum.user_uuid;
          // var color;
          // var sex = parseInt(datum.sex);
          // if(sex === 0){
            // color = new THREE.Color(0x0099FF);
          // }else if(sex === 1){
           // color = new THREE.Color(0xFF3399);
          // }else {
            // color = new THREE.Color(0x9933FF);
          // }
          // for(var i=0; i<N; i++){
            // var _vertex = geometry.vertices[i];
            // if(!_vertex.user_uuid){
              // // _vertex = vertex;
              // _vertex.user_uuid = datum.user_uuid;
              // _vertex.x = vertex.x;
              // _vertex.y = vertex.y;
              // // console.log("new person at the index of " + i);
              // geometry.colors[i] = color;
              // break;
            // }
          // }

          // geometry.verticesNeedUpdate = true;
          // geometry.colorsNeedUpdate = true;
        // }
        // peppers[datum.user_uuid] = {
          // user_uuid: datum.user_uuid,
          // device: datum.device,
          // sex: parseInt(datum.sex),
          // longitude: parseFloat(datum.longitude),
          // latitude: parseFloat(datum.latitude),
          // date: new Date(datum.date)
        // };
      // });
    // });

    function update(){
      layer.render();
    }

    function updatePeppers(){
      var peppers_to_delete = [];
      var current_time = Date.now();
      // Object.keys(peppers).forEach(function(key){
        // var person = peppers[key];
        // sex[person.sex]++;
        // device[person.device]++;
        // console.log(current_time - person.date);
        // if(person.date - current_time > particleDuration){
          // peppers_to_delete.push(key);
        // }
      // });
      // $('.sex-male-num').text(sex[0]);
      // $('.sex-female-num').text(sex[1]);
      // $('.sex-unknown-num').text(sex[2]);
      // $('.device-ios-num').text(device.iphone);
      // $('.device-android-num').text(device.android);

      // 試しにコメントアウト
      // peppers_to_delete.forEach(function(key){
      //   console.log("delete " + key);
      //   delete peppers[key];
      //   for(var i=0; i<N; i++){
      //     delete geometry.vertices[i].user_uuid;
      //     geometry.vertices[i].set(-10, -10, 0);
      //   }
      // });
    }

    function updateVertices(){
      // console.log(geometry.vertices.length);
      // console.log("x: " + geometry.vertices[0].x + "\ty: " + geometry.vertices[0].y);
      // console.log(Object.keys(peppers).length);
      // console.log(particles.geometry);
      for(var i=0; i<N; i++){
        var vertex = geometry.vertices[i];
        if(!vertex.user_uuid) continue;
        var person = peppers[vertex.user_uuid];
        if(!person){
          continue;
        }
        if(Math.abs(person.latitude) < 1 && Math.abs(person.longitude) < 1){
            vertex.set(-10, -10, 0);
        }else{
          var location = new google.maps.LatLng(person.latitude, person.longitude);
          var _vertex = layer.fromLatLngToVertex(location);
          vertex.x = _vertex.x;
          vertex.y = _vertex.y;
        }
      }

      geometry.verticesNeedUpdate = true;
      // geometry.elementsNeedUpdate = true;
      geometry.colorsNeedUpdate = true;
      geometry.computeBoundingSphere();

      layer.render();
    }

    // gui.add(material, 'size', 2, 100).onChange(update);
    // gui.add(material, 'opacity', 0.1, 1).onChange(update);

    // function showVars(){
      // console.log(peppers);
      // console.log(particles);
    // }

    setInterval(updateVertices, 1000/20);
    // setTimeout(showVars, 2000);
    setInterval(updatePeppers, 1000);
  });
}

function generateSprite(size) {

  var canvas = document.createElement('canvas'),
  context = canvas.getContext('2d'),
  gradient;
  size = size || 70;
  canvas.width = size;
  canvas.height = size;

  gradient = context.createRadialGradient(
    canvas.width / 2, canvas.height / 2, 0,
    canvas.width / 2, canvas.height / 2, canvas.width / 2
  );

  gradient.addColorStop(1.0, 'rgba(255,255,255,0)');
  gradient.addColorStop(0.0, 'rgba(255,255,255,1)');

  context.fillStyle = gradient;
  context.fillRect(0, 0, canvas.width, canvas.height);

  return canvas;
}

$(document).ready(init);
