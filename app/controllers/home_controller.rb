class HomeController < ApplicationController
  def index
  end

  def pepper_nearby
    @peppers = Pepper.near([35.620955, 139.748678], 0.1, :units => :km)
  end
end
