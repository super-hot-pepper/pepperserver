class Agent < ActiveRecord::Base
  belongs_to :agent_side
  delegate :side, to: :agent_slide
  belongs_to :pepper, foreign_key: :last_agent_id
end
