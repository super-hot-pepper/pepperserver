class Pepper < ActiveRecord::Base
  belongs_to :pepper_side
  delegate :side, to: :pepper_side
  has_one :last_agent, class_name: "Agent", foreign_key: :last_agent_id

  geocoded_by :address, :latitude  => :lat, :longitude => :long # ActiveRecord
  class << self
    def within_box(distance, latitude, longitude)
      distance = distance # 単位はマイル
      center_point = [latitude, longitude] # 緯度経度
      box = Geocoder::Calculations.bounding_box(center_point, distance)
      self.within_bounding_box(box)
    end
  end
end
