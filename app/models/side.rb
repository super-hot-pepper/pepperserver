class Side < ActiveRecord::Base
  has_many :pepper_sides
  has_many :peppers, through: :pepper_sides
  has_many :agent_sides
  has_many :agents, through: :agent_sides
end
