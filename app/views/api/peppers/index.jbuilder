json.item_count @peppers.size
json.peppers do |json|
  json.array!(@peppers) do |pepper|
    json.id pepper.id
    json.name pepper.name
    json.heart_points pepper.heart_points
    json.lat pepper.lat
    json.long pepper.long
    json.last_agent_id pepper.last_agent_id
    json.last_fight_time pepper.last_fight_time
    json.side pepper.side.name
  end
end
