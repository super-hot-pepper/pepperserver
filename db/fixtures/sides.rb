Side.seed do |s|
  s.id = 1
  s.name = "neutral"
end

Side.seed do |s|
  s.id = 2
  s.name = "dark"
end

Side.seed do |s|
  s.id = 3
  s.name = "light"
end

PepperSide.seed do |s|
  s.id = 1
  s.side_id = 1
end

PepperSide.seed do |s|
  s.id = 2
  s.side_id = 2
end

PepperSide.seed do |s|
  s.id = 3
  s.side_id = 3
end

AgentSide.seed do |s|
  s.id = 1
  s.side_id = 2
end

AgentSide.seed do |s|
  s.id = 2
  s.side_id = 3
end

Agent.seed do |s|
  s.id = 1
  s.name = "John"
  s.agent_side_id = 1
end

Agent.seed do |s|
  s.id = 2
  s.name = "Alice"
  s.agent_side_id = 2
end

names = %w(John Alice Taro Hanako)
100.times do |i|
  Pepper.seed do |s|
    s.id = i+1
    s.name = names.sample + "_" + rand(100).to_s
    r = rand()
    if r < 0.4 then
      s.pepper_side_id = 1
      s.heart_points = (rand(19)-9)
    elsif r < 0.7
      s.pepper_side_id = 2
      s.heart_points = 10 + rand(100)
    else
      s.pepper_side_id = 3
      s.heart_points = -10 - rand(100)
    end
    s.lat = 35.620955 + (rand() * (rand() < 0.5 ? 1 : -1)) * 0.005
    s.long = 139.748678 + (rand() * (rand() < 0.5 ? 1 : -1)) * 0.005
  end
end

