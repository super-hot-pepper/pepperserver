class CreatePepperSides < ActiveRecord::Migration
  def change
    create_table :pepper_sides do |t|
      t.integer :side_id, index: true

      t.timestamps null: false
    end
  end
end
