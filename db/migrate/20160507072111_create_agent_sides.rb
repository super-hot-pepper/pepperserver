class CreateAgentSides < ActiveRecord::Migration
  def change
    create_table :agent_sides do |t|
      t.integer :side_id, index: true

      t.timestamps null: false
    end
  end
end
