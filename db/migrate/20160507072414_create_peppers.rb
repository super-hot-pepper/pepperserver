class CreatePeppers < ActiveRecord::Migration
  def change
    create_table :peppers do |t|
      t.string :name
      t.integer :pepper_side_id, index: true
      t.integer :heart_points, default: 0
      t.float :lat, limit: 53
      t.float :long, limit: 53

      t.timestamps null: false
    end
  end
end
