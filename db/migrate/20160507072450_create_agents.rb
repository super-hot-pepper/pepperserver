class CreateAgents < ActiveRecord::Migration
  def change
    create_table :agents do |t|
      t.string :random_id
      t.string :name
      t.text :bio
      t.integer :agent_side_id, index: true
      t.integer :force, default: 0
      t.integer :level, default: 0

      t.timestamps null: false
    end
  end
end
