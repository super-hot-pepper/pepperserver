class AddLastfightToPeppers < ActiveRecord::Migration
  def change
    add_column :peppers, :last_agent_id, :integer
    add_column :peppers, :last_fight_time, :datetime
  end
end
