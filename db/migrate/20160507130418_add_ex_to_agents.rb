class AddExToAgents < ActiveRecord::Migration
  def change
    add_column :agents, :ex, :integer, default: 0
  end
end
