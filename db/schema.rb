# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20160507130418) do

  create_table "agent_sides", force: :cascade do |t|
    t.integer  "side_id",    limit: 4
    t.datetime "created_at",           null: false
    t.datetime "updated_at",           null: false
  end

  add_index "agent_sides", ["side_id"], name: "index_agent_sides_on_side_id", using: :btree

  create_table "agents", force: :cascade do |t|
    t.string   "random_id",     limit: 255
    t.string   "name",          limit: 255
    t.text     "bio",           limit: 65535
    t.integer  "agent_side_id", limit: 4
    t.integer  "force",         limit: 4,     default: 0
    t.integer  "level",         limit: 4,     default: 0
    t.datetime "created_at",                              null: false
    t.datetime "updated_at",                              null: false
    t.integer  "ex",            limit: 4,     default: 0
  end

  add_index "agents", ["agent_side_id"], name: "index_agents_on_agent_side_id", using: :btree

  create_table "items", force: :cascade do |t|
    t.string   "name",       limit: 255
    t.string   "type",       limit: 255
    t.string   "image_url",  limit: 255
    t.datetime "created_at",             null: false
    t.datetime "updated_at",             null: false
  end

  create_table "pepper_sides", force: :cascade do |t|
    t.integer  "side_id",    limit: 4
    t.datetime "created_at",           null: false
    t.datetime "updated_at",           null: false
  end

  add_index "pepper_sides", ["side_id"], name: "index_pepper_sides_on_side_id", using: :btree

  create_table "peppers", force: :cascade do |t|
    t.string   "name",            limit: 255
    t.integer  "pepper_side_id",  limit: 4
    t.integer  "heart_points",    limit: 4,   default: 0
    t.float    "lat",             limit: 53
    t.float    "long",            limit: 53
    t.datetime "created_at",                              null: false
    t.datetime "updated_at",                              null: false
    t.integer  "last_agent_id",   limit: 4
    t.datetime "last_fight_time"
  end

  add_index "peppers", ["pepper_side_id"], name: "index_peppers_on_pepper_side_id", using: :btree

  create_table "sides", force: :cascade do |t|
    t.string   "name",        limit: 255
    t.text     "description", limit: 65535
    t.datetime "created_at",                null: false
    t.datetime "updated_at",                null: false
  end

end
