require 'test_helper'

class HomeControllerTest < ActionController::TestCase
  test "should get index" do
    get :index
    assert_response :success
  end

  test "should get pepper_nearby" do
    get :pepper_nearby
    assert_response :success
  end

end
